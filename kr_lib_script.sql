--��� ������� ��� ���� �������
create table test_index(
id int not null,
pole1 char(36) not null,
pole2 char(216) not null
)


--��������� ���������������� �������
truncate table test_index
create clustered index idx_cl_id on test_index(id)

declare @i as int=0
while @i<18630
begin
	set @i = @i+1;
	insert into test_index
		values(@i, 'a','b')
	end;

--���������� �� ��������
select OBJECT_NAME(object_id) as table_name,
name as index_name, type, type_desc
from sys.indexes
where OBJECT_ID = OBJECT_ID(N'test_index')
exec dbo.sp_spaceused @objname = N'test_index', @updateusage = true;

--��������� ������������������ �������
create nonclustered index ix_date_rozm on test_index(id)
with  fillfactor  = 70

--�������� ��������� �������
select OBJECT_NAME(object_id) as table_name,
name as index_name, type, type_desc
from sys.indexes
where OBJECT_ID = OBJECT_ID(N'test_index')


--��������� �����
CREATE LOGIN administrator_kr WITH PASSWORD = '111'
--��������� �����
CREATE USER administrator_kr FOR LOGIN administrator_kr

--��������� ��� 1
CREATE ROLE administrator_1
ALTER ROLE administrator_1 ADD MEMBER administrator_kr
--��������� ����
GRANT SELECT, INSERT, UPDATE ON Book_published TO administrator_1
GRANT SELECT, INSERT, UPDATE ON Book TO administrator_1
GRANT SELECT, INSERT, UPDATE ON Reader TO administrator_1

--��������� ��� 2
CREATE ROLE administrator_2
ALTER ROLE administrator_2 ADD MEMBER administrator_kr
--��������� ����
GRANT SELECT, INSERT, UPDATE ON Book_published TO administrator_2
GRANT SELECT, INSERT, UPDATE ON Book TO administrator_2
GRANT SELECT, INSERT, UPDATE ON Reader TO administrator_2

--��������� ��� 3
CREATE ROLE administrator_3
ALTER ROLE administrator_3 ADD MEMBER administrator_kr
--��������� ����
GRANT SELECT, INSERT, UPDATE ON Book_published TO administrator_3
GRANT SELECT, INSERT, UPDATE ON Book TO administrator_3
GRANT SELECT, INSERT, UPDATE ON Reader TO administrator_3



-- � ��������� ���� ��
SELECT *
INTO Book_copy
FROM Book
WHERE 1 <> 1;

SELECT * FROM Book_copy
truncate table Book_copy

--��� �����
CREATE ASYMMETRIC KEY ASymKeyEnc
WITH ALGORITHM = RSA_2048
ENCRYPTION BY PASSWORD = '111'
--���������� ����� ������
INSERT dbo.Book_copy (ID_book, Name, Author, Year, Cost, Amount) VALUES (1, '����� �������� �������������� ������', EncryptByAsymKey(ASYMKEY_ID('ASymKeyEnc'),
Convert(nvarchar(100), '���� �������')), 2006-01-01, 150, 5);
INSERT dbo.Book_copy (ID_book, Name, Author, Year, Cost, Amount) VALUES (2, '��������', EncryptByAsymKey(ASYMKEY_ID('ASymKeyEnc'),
Convert(nvarchar(100), '��� �����')), 2019-01-14, 235, 10);
INSERT dbo.Book_copy (ID_book, Name, Author, Year, Cost, Amount) VALUES (3, '������ ��������� ���������', EncryptByAsymKey(ASYMKEY_ID('ASymKeyEnc'),
Convert(nvarchar(100), '̺����')), 1975-05-04, 75, 7);

SELECT * FROM Book_copy

SELECT Convert(nvarchar(50), DecryptByAsymKey(AsymKey_ID('ASymKeyEnc'), Author, N'111'))
FROM Book_copy

--��������� ������
BACKUP DATABASE restaurant_chain
TO DISK = 'd:\Lib.bak' WITH INIT, 
NAME = 'Lib backup',
DESCRIPTION = 'Lib backup'

--³��������� ��
RESTORE DATABASE Lib
FROM DISK = 'd:\Lib.bak'
WITH RECOVERY


