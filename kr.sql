CREATE DATABASE Lib

--��������� �� �� �������

CREATE TABLE [Book]
(
 [ID_book] INT NOT NULL ,
 [Name]    VARCHAR(50) NOT NULL ,
 [Author]  VARCHAR(50) NOT NULL ,
 [Year]    DATETIME NOT NULL ,
 [Cost]    INT NOT NULL ,
 [Amount]  SMALLINT NOT NULL ,


 CONSTRAINT [PK_book] PRIMARY KEY CLUSTERED ([ID_book] ASC)
);
GO

CREATE TABLE [Reader]
(
 [ID_reader] INT NOT NULL ,
 [Initials]  VARCHAR(100) NOT NULL ,
 [Address]   VARCHAR(50) NOT NULL ,
 [Number]    VARCHAR(13) NOT NULL ,
 [Del]    BIT NOT NULL ,


 CONSTRAINT [PK_reader] PRIMARY KEY CLUSTERED ([ID_reader] ASC)
);
GO

CREATE TABLE [Book_published]
(
 [Date_publishing]  DATETIME NOT NULL ,
 [Number_order]     INT NOT NULL ,
 [Real_return_date] DATETIME NOT NULL ,
 [Plan_return_date] DATETIME NOT NULL ,
 [ID_reader]        INT NOT NULL ,
 [ID_book]          INT NOT NULL ,


 CONSTRAINT [PK_book_published] PRIMARY KEY CLUSTERED ([Number_order] ASC),
 CONSTRAINT [FK_155] FOREIGN KEY ([ID_reader])  REFERENCES [Reader]([ID_reader]),
 CONSTRAINT [FK_158] FOREIGN KEY ([ID_book])  REFERENCES [Book]([ID_book])
);
GO


CREATE NONCLUSTERED INDEX [fkIdx_155] ON [Book_published] 
 (
  [ID_reader] ASC
 )

GO

CREATE NONCLUSTERED INDEX [fkIdx_158] ON [Book_published] 
 (
  [ID_book] ASC
 )

GO

--���������� ������


INSERT dbo.Book (ID_book, Name, Author, Year, Cost, Amount) VALUES (0, '����� �������� �������������� ������', '���� �������', 2006-01-01, 150, 5);
INSERT dbo.Book (ID_book, Name, Author, Year, Cost, Amount) VALUES (1, '��������', '��� �����', 2019-01-14, 235, 10);
INSERT dbo.Book (ID_book, Name, Author, Year, Cost, Amount) VALUES (2, '������ ��������� ���������', '���� ̺����', 1975-05-04, 75, 7);

SELECT * FROM Book

INSERT dbo.Reader (ID_reader, Initials, Address, Number, Del) VALUES (0, '������ �.�.', 'pekariev', '+380638591132', 0);
INSERT dbo.Reader (ID_reader, Initials, Address, Number, Del) VALUES (1, '����� �.�.', 'anchis', '+380672231869', 0);
INSERT dbo.Reader (ID_reader, Initials, Address, Number, Del) VALUES (2, '������ �.�.', 'plitus', '+380938375194', 0);

SELECT * FROM Reader

INSERT dbo.Book_published (Number_order, Date_publishing, Plan_return_date, Real_return_date, ID_reader, ID_book) 
VALUES (0, '2000-03-01', '2000-04-01', '2000-09-05', 0, 0);
INSERT dbo.Book_published (Number_order, Date_publishing, Plan_return_date, Real_return_date, ID_reader, ID_book) 
VALUES (1, '2000-04-05', '2000-05-05', '2000-06-11', 1, 0);
INSERT dbo.Book_published (Number_order, Date_publishing, Plan_return_date, Real_return_date, ID_reader, ID_book) 
VALUES (2, '2001-08-07', '2001-09-07', '2001-09-01', 1, 1);
INSERT dbo.Book_published (Number_order, Date_publishing, Plan_return_date, Real_return_date, ID_reader, ID_book) 
VALUES (3, '2000-08-07', '2000-09-07', '2000-12-12', 1, 2);



SELECT * FROM Book
SELECT * FROM Book_published
SELECT * FROM Reader
